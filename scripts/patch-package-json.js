const {
  writeFileSync,
  readFileSync
} = require('fs');
const {
  resolve
} = require('path');

const pkgPath = resolve(__dirname + '/../pkg/release');

const artifactVersions = [{
  name: 'esm', // this is --target bundler
  type: 'module'
}, {
  name: 'cjs',
  type: 'commonjs'
}, {
  name: 'web',
  type: 'module'
}, {
  name: 'no-modules',
  type: 'module'
},]

function main() {
  artifactVersions.forEach(a => {
    const pkgJsonPath = [pkgPath, a.name, 'package.json'].join('/')
    const imported = readFileSync(pkgJsonPath).toString();
    const parsed = JSON.parse(imported)
    let clonedImported = Object.assign({}, parsed);

    if (clonedImported.hasOwnProperty('type')) {
      // console.log(`TYPE exists and it is %s`, clonedImported.type);
    } else {
      // console.log(`TYPE does not exists for ${a.name} adding it ...`);
      clonedImported.type = a.type;

      const newData = JSON.stringify(clonedImported, null, 2);

      writeFileSync(pkgJsonPath, newData);
      // console.log('done')
    }
  })
}

main()