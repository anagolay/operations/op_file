# op_file

`op_file` is an Anagolay Operation that provides access to a remote file given its URL. It performs an  
asynchronous HTTP request to fetch the file and provides its `Bytes` as output, according to its manifest data:
```
{
    "name": "op_file",
    "description": "Reads the file from given url and returns the buffer. RAW file buffer for other ops to use.",
    "inputs": [
        "String"
    ],
    "config": {
    },
    "groups": [
        "SYS"
    ],
    "output": "Bytes",
    "repository": "https://gitlab.com/anagolay/operations/op_file",
    "license": "Apache 2.0",
    "features": [
    ],
}
```

## Installation

In the Cargo.toml file of your Workflow, specify a dependency toward **op_file** crate:

```toml
[dependencies]
op_file = {  git = "https://gitlab.com/anagolay/operations/op_file" }
```
**NOTE:** the usage of this crate in a no-std environment is not possible.

## Features

* **anagolay** Used to build this crate as dependency for other operations (default)
* **js** Used to build the javascript bindings (default)
* **debug_assertions** Used to produce helpful debug messages (default)
* **test** Used to build and execute tests

## Usage

### Execution

From Rust: 

```rust
use op_file;

let input = "https://bafybeiavjzfgrxx2zq5r3vx352amhuzdv5pc5cu32xp7tlh4iqvcuxjcze.ipfs.dweb.link/tenerife-light-painting-01-1000x1000.jpg";
let config = std::collections::BTreeMap::new();
let output = op_file::execute(input, config).await.unwrap();
```
Or in WASM environment:

```javascript
import { execute } from "@anagolay/op_file"

async function main() {
  const input = "https://bafybeiavjzfgrxx2zq5r3vx352amhuzdv5pc5cu32xp7tlh4iqvcuxjcze.ipfs.dweb.link/tenerife-light-painting-01-1000x1000.jpg"
  const config = {}
  let output = await execute([input], config)
}
```

**NOTE:** 
In nodejs WASM will use [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API).
In order to perform the HTTP request successfully, it is necessary to polyfill it by mean of `node-fetch` or similar.
This must happen before execution, so the previous snippet must be preceded by the following lines:

```javascript
// Polyfill fetch
import fetch from 'node-fetch';
import {Headers, Request, Response} from 'node-fetch';
global.fetch = fetch
global.Headers = Headers
global.Request = Request
global.Response = Response
```

## Development

It is suggested to use the Vscode `.devcontainer` or any other sandboxed envirnoment like Gitpod beacuse the Operations must be developed under the same enviroment as they are published. 

Git hooks will be initialized on first build or a test. If you wish to init them before run this in your terminal:

```shell
rusty-hook init
```

To help you with writing consitent code we provide you with following: 

- `.gitlab-ci.yml` for your testing and fmt checks
- `rusty-hook.toml` for the `pre-push` fmt check so your CI is green.


### Manifest generation

```shell
makers manifest
```

## License
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)